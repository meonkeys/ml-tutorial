#!/bin/bash

# exit with error if anything returns nonzero exit status
# (disable to make sure final xhost setting cleanup occurs)
#set -o errexit

set -o nounset
set -o pipefail

# start interactive shell and mount current dir for instant local edits
echo ">>> test with 'python /live/hello.py' <<<"

set -o xtrace

# allow use of host X11 display
xhost +
XSOCK=/tmp/.X11-unix

# run non-interactive
#docker run -it --rm -v $XSOCK:$XSOCK --name ml-tutorial ml-tutorial
docker run -it --rm -v $XSOCK:$XSOCK -v "$(pwd):/live" --name ml-tutorial ml-tutorial /bin/bash

# lock back down X11 access
xhost -
