#!/bin/bash

set -o errexit
set -o nounset
set -o pipefail

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

set -o xtrace
cd "$DIR"
# add --pull to always get the latest upstream/parent image
docker build -t ml-tutorial .
