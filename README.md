Dockerized workspace and completed example code for [Jason Brownlee's machine learning tutorial](http://machinelearningmastery.com/machine-learning-in-python-step-by-step/).

Use `rebuild.sh` to create a Docker image for local dev. Use `./run.sh` to start a container from that image.

Tested on 64-bit Ubuntu 14.04 LTS desktop with Docker 1.12.1.

Other notes:

* `requirements.txt` lists [pip packages](https://en.wikipedia.org/wiki/Pip_(package_manager) that will be installed during image creation (when `rebuild.sh` is run). See [the python repository page on Docker Hub](https://hub.docker.com/_/python/) for details.
* data is from <https://en.wikipedia.org/wiki/Iris_flower_data_set>
* numpy arrays
    * array references can include commas to indicate selection by column
    * <http://docs.scipy.org/doc/numpy/reference/arrays.indexing.html#advanced-indexing>
    * <http://stackoverflow.com/questions/17370820/python-slice-notation-with-comma-list/17371453#17371453>
* statistics
    * <https://en.wikipedia.org/wiki/Normal_distribution>
